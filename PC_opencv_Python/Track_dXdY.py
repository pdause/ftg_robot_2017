import numpy as np
import cv2
import time
from collections import deque

cap = cv2.VideoCapture(1)

cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

time.sleep(1)

cap.set(cv2.CAP_PROP_EXPOSURE, -9.0)

pts = deque(maxlen=64)
counter = 0
(dX, dY) = (0, 0)
direction = ""

def nothing(x):
    pass
# Creating a window for later use
cv2.namedWindow('result')

# Starting with 100's to prevent error while masking
h,s,v = 100,100,100

# Creating track bar
cv2.createTrackbar('h_l', 'result',0,179,nothing)
cv2.createTrackbar('h_h', 'result',179,179,nothing)
cv2.createTrackbar('s_l', 'result',0,255,nothing)
cv2.createTrackbar('s_h', 'result',255,255,nothing)
cv2.createTrackbar('v_l', 'result',0,255,nothing)
cv2.createTrackbar('v_h', 'result',255,255,nothing)
while(1):

    _, frame = cap.read()

    #converting to HSV
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (5, 5), 0)
    edged = cv2.Canny(gray, 35, 125)
    
    hsv = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)

    # get info from track bar and appy to result
    h_low = cv2.getTrackbarPos('h_l','result')
    s_low = cv2.getTrackbarPos('s_l','result')
    v_low = cv2.getTrackbarPos('v_l','result')

    lower_color = np.array([h_low,s_low,v_low])

    h_Hi = cv2.getTrackbarPos('h_h','result')
    s_Hi = cv2.getTrackbarPos('s_h','result')
    v_Hi = cv2.getTrackbarPos('v_h','result')

    upper_color = np.array([h_Hi,s_Hi,v_Hi])

    mask = cv2.inRange(hsv,lower_color, upper_color)

    result = cv2.bitwise_and(frame,frame,mask = mask)

    #ret,thresh = cv2.threshold(mask,127,255,0)
    #ret,thresh = cv2.adaptiveThreshold(
    cnts = cv2.findContours(mask,cv2.RETR_EXTERNAL,
                                                cv2.CHAIN_APPROX_SIMPLE)[-2]

    #area = cv2.contourArea(cnt)

    #c = max(
    if len(cnts) > 0:
        c = max( cnts, key=cv2.contourArea)
        M = cv2.moments(c)

        if M['m00'] > 0:
            center = (int(M['m10']/M['m00']),int(M['m01']/M['m00']))
            
            rect = cv2.minAreaRect(c)
            box = cv2.boxPoints(rect)
            box = np.int0(box)
            cv2.drawContours(result,[box],0,(0,0,255),2)
            cv2.circle(result, center, 5, (0, 0, 255), -1)
            pts.appendleft(center)

        # loop over the set of tracked points
        for i in np.arange(1, len(pts)):
            if pts[i - 1] is None or pts[i] is None:
                continue

            if counter >= 10 and i == 1 and pts[-10] is not None:
                dX = pts[-10][0] - pts[i][0]
                dY = pts[-10][1] - pts[i][1]
                (dirX, dirY) = ("", "")
                
                # ensure there is significant movement in the
                # x-direction
                if np.abs(dX) > 20:
                    dirX = "East" if np.sign(dX) == 1 else "West"

                # ensure there is significant movement in the
                # y-direction
                if np.abs(dY) > 20:
                    dirY = "North" if np.sign(dY) == 1 else "South"

                # handle when both directions are non-empty
                if dirX != "" and dirY != "":
                    direction = "{}-{}".format(dirY, dirX)

                # otherwise, only one direction is non-empty
                else:
                    direction = dirX if dirX != "" else dirY

			
            # otherwise, compute the thickness of the line and
            # draw the connecting lines
            thickness = int(np.sqrt(64 / float(i + 1)) * 2.5)
            cv2.line(result, pts[i - 1], pts[i], (0, 0, 255), thickness)

    # show the movement deltas and the direction of movement on
    # the frame
    cv2.putText(result, direction, (10, 30), cv2.FONT_HERSHEY_SIMPLEX,
                0.65, (0, 0, 255), 3)
    cv2.putText(result, "dx: {}, dy: {}".format(dX, dY),
                (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX,
                0.35, (0, 0, 255), 1)

    cv2.imshow('box',result)

    k = cv2.waitKey(1) & 0xFF
    if k == 27:
        break
    counter += 1

cap.release()
cv2.destroyAllWindows()
