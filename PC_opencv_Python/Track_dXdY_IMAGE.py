import numpy as np
import cv2
import time
from collections import deque

pts = deque(maxlen=64)
counter = 0
(dX, dY) = (0, 0)
direction = ""

def nothing(x):
    pass
# Creating a window for later use
cv2.namedWindow('result')

# Starting with 100's to prevent error while masking
h,s,v = 100,100,100

# Creating track bar
##cv2.createTrackbar('h_l', 'result',50,179,nothing)
##cv2.createTrackbar('h_h', 'result',100,179,nothing)
##cv2.createTrackbar('s_l', 'result',100,255,nothing)
##cv2.createTrackbar('s_h', 'result',255,255,nothing)
##cv2.createTrackbar('v_l', 'result',70,255,nothing)
##cv2.createTrackbar('v_h', 'result',255,255,nothing)

images =( r'.\LED Peg\1ftH1ftD0Angle0Brightness.jpg',
          r'.\LED Peg\1ftH1ftD1Angle0Brightness.jpg',
          r'.\LED Peg\1ftH2ftD0Angle0Brightness.jpg',
          r'.\LED Peg\1ftH2ftD1Angle0Brightness.jpg',
          r'.\LED Peg\1ftH2ftD2Angle0Brightness.jpg',
          r'.\LED Peg\1ftH3ftD0Angle0Brightness.jpg',
          r'.\LED Peg\1ftH3ftD2Angle0Brightness.jpg',
          r'.\LED Peg\1ftH4ftD0Angle0Brightness.jpg',
          r'.\LED Peg\1ftH4ftD2Angle0Brightness.jpg',
          r'.\LED Peg\1ftH4ftD3Angle0Brightness.jpg',
          r'.\LED Peg\1ftH5ftD0Angle0Brightness.jpg',
          r'.\LED Peg\1ftH5ftD2Angle0Brightness.jpg',
          r'.\LED Peg\1ftH5ftD3Angle0Brightness.jpg',
          r'.\LED Peg\1ftH6ftD0Angle0Brightness.jpg',
          r'.\LED Peg\1ftH7ftD0Angle0Brightness.jpg',
          r'.\LED Peg\1ftH8ftD0Angle0Brightness.jpg',
          r'.\LED Peg\1ftH9ftD0Angle0Brightness.jpg' )

cv2.createTrackbar('Pic', 'result',0,len(images)-1,nothing)

while True:

    Curr_Image = images[cv2.getTrackbarPos('Pic','result')]
    frame = cv2.imread(Curr_Image)
    
    #converting to HSV
    hsv = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
    
    # get info from track bar and appy to result
    h_low = 50 #cv2.getTrackbarPos('h_l','result')
    s_low = 100 #cv2.getTrackbarPos('s_l','result')
    v_low = 70 #cv2.getTrackbarPos('v_l','result')

    lower_color = np.array([h_low,s_low,v_low])

    h_Hi = 100 #cv2.getTrackbarPos('h_h','result')
    s_Hi = 255 #cv2.getTrackbarPos('s_h','result')
    v_Hi = 255 #cv2.getTrackbarPos('v_h','result')

    upper_color = np.array([h_Hi,s_Hi,v_Hi])
    mask = cv2.inRange(hsv,lower_color, upper_color)
    result = cv2.bitwise_and(frame,frame,mask = mask)

    
    #This will find rectangles based on the threshold of the grayscale image
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    thresh = cv2.threshold(gray, 20, 255, cv2.THRESH_BINARY)[1]
    cv2.imshow('frame',thresh)
    cnts = cv2.findContours(thresh,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)[-2]
    cv2.drawContours(result,cnts,-1,(0,255,0),3)
    Saved_Contours = []
    for c in cnts:
        #This gives the perimeter
        peri = cv2.arcLength(c, True)
        #This gives a contour approximation for shape
        approx = cv2.approxPolyDP(c, 0.04 * peri, True)
        #We have a square or a rectangle because there are 4 vertices
        if len(approx) == 4:
            (x, y, w, h) = cv2.boundingRect(approx)
            #Aspect ratio for a rectangle is typically w / h since our rectangles
            #height is horizontal
            AspectRatio = float(h) / w
            if AspectRatio <= 3.0 and AspectRatio >= 2.0:
                M = cv2.moments(c)
                if M["m00"] > 0:
                    cX = int((M["m10"] / M["m00"]))
                    cY = int((M["m01"] / M["m00"]))
                    #cv2.putText(result, "AR", (cX, cY + 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 3)
                    #rect = cv2.minAreaRect(c)
                    #box = cv2.boxPoints(rect)
                    #box = np.int0(box)
                    #cv2.drawContours(result,[box],0,(0,0,127),2)
                    #cv2.circle(result, (cX,cY), 5, (0, 0, 127), -1)
                    Saved_Contours.append(c)
                    
    Largest_Area = 0
    Second_Largest_Area = 0
    Largest_Contour = 0
    Second_Largest_Contour = 0
    for contours in Saved_Contours:
        area = cv2.contourArea(contours)
        if area > Largest_Area:
            if Largest_Area:
                Second_Largest_Area = Largest_Area
                Second_Largest_Contour = Largest_Contour
            Largest_Area = area
            Largest_Contour = contours
        elif area > Second_Largest_Area:
            Second_Largest_Area = area
            Second_Largest_Contour = contours

    if Largest_Area and (Second_Largest_Area / Largest_Area) < 0.65:
        Second_Largest_Area = 0
        Second_Largest_Contour = 0

    if Largest_Area and Second_Largest_Area:
        rectL = cv2.minAreaRect(Largest_Contour)
        rectSL = cv2.minAreaRect(Second_Largest_Contour)
        #This is shit redo all of this to find the lowest and highest x,y
        #and then plot the recangle based on the real coordinates
        leftmostL = tuple(Largest_Contour[Largest_Contour[:,:,0].argmin()][0])
        rightmostL = tuple(Largest_Contour[Largest_Contour[:,:,0].argmax()][0])
        topmostL = tuple(Largest_Contour[Largest_Contour[:,:,1].argmin()][0])
        bottommostL = tuple(Largest_Contour[Largest_Contour[:,:,1].argmax()][0])
        leftmostSL = tuple(Second_Largest_Contour[Second_Largest_Contour[:,:,0].argmin()][0])
        rightmostSL = tuple(Second_Largest_Contour[Second_Largest_Contour[:,:,0].argmax()][0])
        topmostSL = tuple(Second_Largest_Contour[Second_Largest_Contour[:,:,1].argmin()][0])
        bottommostSL = tuple(Second_Largest_Contour[Second_Largest_Contour[:,:,1].argmax()][0])

        
        cv2.circle(result, leftmostL, 5, (0, 0, 100), -1)
        cv2.circle(result, rightmostL, 5, (0, 0, 100), -1)
        cv2.circle(result, bottommostL, 5, (0, 0, 100), -1)
        cv2.circle(result, topmostL, 5, (0, 0, 100), -1)
##        cv2.putText(result, "lmL", leftmostL, cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 3)
##        cv2.putText(result, "rmL", rightmostL, cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 3)
##        cv2.putText(result, "bmL", bottommostL, cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 3)
##        cv2.putText(result, "tmL", topmostL, cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 3)
		
        #cv2.putText(result, "lmtmSL", (leftmostSL, topmostSL), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 3)
        #cv2.putText(result, "lmbmSL", (leftmostSL, bottommostSL), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 3)
        #cv2.putText(result, "rmtmSL", (rightmostSL, topmostSL), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 3)
        #cv2.putText(result, "rmbmSL", (rightmostSL, bottommostSL), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 3)
        
            
        centerX = (rectL[0][0] + rectSL[0][0]) / 2
        centerY = (rectL[0][1] + rectSL[0][1]) / 2
        Width = (rectL[1][0] / 2) + (rectSL[1][0] / 2) +\
                ((abs(centerX - rectL[0][0])) * 2)
        height = rectL[1][1]
        angle = rectL[2]

        rectangle = ((centerX,centerY),(Width,height),angle)
        box = cv2.boxPoints(rectangle)
        box = np.int0(box)
        cv2.drawContours(result,[box],0,(0,0,255),2)
        center = (int(centerX),int(centerY))
        cv2.circle(result, center, 5, (0, 0, 255), -1)
    else:
        M = cv2.moments(Largest_Contour)
        if M["m00"] > 0:
            cX = int((M["m10"] / M["m00"]))
            cY = int((M["m01"] / M["m00"]))
            cv2.putText(result, "TAPE", (cX, cY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 3)
            rect = cv2.minAreaRect(Largest_Contour)
            box = cv2.boxPoints(rect)
            box = np.int0(box)
            cv2.drawContours(result,[box],0,(0,0,255),2)
            cv2.circle(result, (cX,cY), 5, (0, 0, 255), -1)
        

    cv2.imshow('result',result)

    k = cv2.waitKey(1) & 0xFF
    if k == 27:
        break

cv2.destroyAllWindows()
