import numpy as np
import cv2
import time
from collections import deque
from skimage.feature import peak_local_max
from skimage.morphology import watershed
from scipy import ndimage

pts = deque(maxlen=64)
counter = 0
(dX, dY) = (0, 0)
direction = ""

def nothing(x):
    pass
# Creating a window for later use
cv2.namedWindow('result')

# Starting with 100's to prevent error while masking
h,s,v = 100,100,100

images =( r'.\LED Peg\1ftH1ftD0Angle0Brightness.jpg',
          r'.\LED Peg\1ftH1ftD1Angle0Brightness.jpg',
          r'.\LED Peg\1ftH2ftD0Angle0Brightness.jpg',
          r'.\LED Peg\1ftH2ftD1Angle0Brightness.jpg',
          r'.\LED Peg\1ftH2ftD2Angle0Brightness.jpg',
          r'.\LED Peg\1ftH3ftD0Angle0Brightness.jpg',
          r'.\LED Peg\1ftH3ftD2Angle0Brightness.jpg',
          r'.\LED Peg\1ftH4ftD0Angle0Brightness.jpg',
          r'.\LED Peg\1ftH4ftD2Angle0Brightness.jpg',
          r'.\LED Peg\1ftH4ftD3Angle0Brightness.jpg',
          r'.\LED Peg\1ftH5ftD0Angle0Brightness.jpg',
          r'.\LED Peg\1ftH5ftD2Angle0Brightness.jpg',
          r'.\LED Peg\1ftH5ftD3Angle0Brightness.jpg',
          r'.\LED Peg\1ftH6ftD0Angle0Brightness.jpg',
          r'.\LED Peg\1ftH7ftD0Angle0Brightness.jpg',
          r'.\LED Peg\1ftH8ftD0Angle0Brightness.jpg',
          r'.\LED Peg\1ftH9ftD0Angle0Brightness.jpg' )

cv2.createTrackbar('Pic', 'result',0,len(images)-1,nothing)

while True:

    Curr_Image = images[cv2.getTrackbarPos('Pic','result')]
    frame = cv2.imread(Curr_Image)
    result = frame
    cv2.imshow("orig", frame)
    
    shifted = cv2.pyrMeanShiftFiltering(frame, 21, 51)
    gray = cv2.cvtColor(shifted, cv2.COLOR_BGR2GRAY)
    thresh = cv2.threshold(gray, 20, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    cv2.imshow("Input", thresh)
    D = ndimage.distance_transform_edt(thresh)
    localMax = peak_local_max(D, indices=False, min_distance=20,labels=thresh)
    markers = ndimage.distance_transform_edt(thresh)

    
    #This will find rectangles based on the threshold of the grayscale image
    cnts = cv2.findContours(thresh,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)[-2]
    cv2.drawContours(result,cnts,-1,(0,255,0),3)
    Saved_Contours = []
    for c in cnts:
        #This gives the perimeter
        peri = cv2.arcLength(c, True)
        #This gives a contour approximation for shape
        approx = cv2.approxPolyDP(c, 0.04 * peri, True)
        #We have a square or a rectangle because there are 4 vertices
        if len(approx) == 4:
            (x, y, w, h) = cv2.boundingRect(approx)
            #Aspect ratio for a rectangle is typically w / h since our rectangles
            #height is horizontal
            AspectRatio = float(h) / w
            if AspectRatio <= 3.0 and AspectRatio >= 2.0:
                M = cv2.moments(c)
                if M["m00"] > 0:
                    cX = int((M["m10"] / M["m00"]))
                    cY = int((M["m01"] / M["m00"]))
                    Saved_Contours.append(c)
                    
    Largest_Area = 0
    Second_Largest_Area = 0
    Largest_Contour = 0
    Second_Largest_Contour = 0
    for contours in Saved_Contours:
        area = cv2.contourArea(contours)
        if area > Largest_Area:
            if Largest_Area:
                Second_Largest_Area = Largest_Area
                Second_Largest_Contour = Largest_Contour
            Largest_Area = area
            Largest_Contour = contours
        elif area > Second_Largest_Area:
            Second_Largest_Area = area
            Second_Largest_Contour = contours

    if Largest_Area and (Second_Largest_Area / Largest_Area) < 0.65:
        Second_Largest_Area = 0
        Second_Largest_Contour = 0

    if Largest_Area and Second_Largest_Area:
        rectL = cv2.minAreaRect(Largest_Contour)
        rectSL = cv2.minAreaRect(Second_Largest_Contour)
        centerX = (rectL[0][0] + rectSL[0][0]) / 2
        centerY = (rectL[0][1] + rectSL[0][1]) / 2
        Width = (rectL[1][0] / 2) + (rectSL[1][0] / 2) +\
                ((abs(centerX - rectL[0][0])) * 2)
        height = rectL[1][1]
        angle = rectL[2]

        rectangle = ((centerX,centerY),(Width,height),angle)
        box = cv2.boxPoints(rectangle)
        box = np.int0(box)
        cv2.drawContours(result,[box],0,(0,0,255),2)
        center = (int(centerX),int(centerY))
        cv2.circle(result, center, 5, (0, 0, 255), -1)
    else:
        M = cv2.moments(Largest_Contour)
        if M["m00"] > 0:
            cX = int((M["m10"] / M["m00"]))
            cY = int((M["m01"] / M["m00"]))
            cv2.putText(result, "TAPE", (cX, cY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 3)
            rect = cv2.minAreaRect(Largest_Contour)
            box = cv2.boxPoints(rect)
            box = np.int0(box)
            cv2.drawContours(result,[box],0,(0,0,255),2)
            cv2.circle(result, (cX,cY), 5, (0, 0, 255), -1)
        

    cv2.imshow('result',result)

    k = cv2.waitKey(1) & 0xFF
    if k == 27:
        break

cv2.destroyAllWindows()
