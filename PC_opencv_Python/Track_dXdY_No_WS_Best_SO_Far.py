import numpy as np
import cv2
import time
from collections import deque
from scipy.spatial import distance as dist
from imutils import perspective
from operator import itemgetter, attrgetter
import itertools

cap = cv2.VideoCapture(0)

cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

time.sleep(1)

cap.set(cv2.CAP_PROP_EXPOSURE, -8.0)

pts = deque(maxlen=64)
counter = 0
(dX, dY) = (0, 0)
direction = ""

class Screen_Obj:
    def __init__(self, Rect, MBRect, Box, Area, Percent_Error_Rectangle ):
        self.Rect = Rect #Rect data same as bounding rect data (x,y,w,h)
        self.MBRect = MBRect #minAreaRect MBRect data is ((Center x,Center y),(w,h),angle)
        self.Box = Box
        self.Area = Area
        self.Percent_Error_Rectangle = Percent_Error_Rectangle
    def __repr__(self):
        return repr((self.Rect),(self.MBRect),(self.Box),(self.Area),(self.Percent_Error_Rectangle))
        

def Two_Target_Compare( T1, T2 ):

    Avg = []
    #Get the ratio of the area of the two rectangles being compared
    if T1.Area > T2.Area:
        area_ratio = T2.Area / T1.Area
    else:
        area_ratio = T1.Area / T2.Area

    #This will compare the center x coords
    if T1.MBRect[0][0] > T2.MBRect[0][0]:
        Left = T2
        Right = T1
    else:
        Left = T1
        Right = T2

    L_P = perspective.order_points(Left.Box)
    R_P = perspective.order_points(Right.Box)

    #Rect data same as bounding rect data (x,y,w,h)
    #minAreaRect MBRect data is ((Center x,Center y),(w,h),angle)
    
    #Find the center width to height percent Calculated is 8.25 / 5 = 1.65
    Avg.append(abs((((Right.MBRect[0][0] - Left.MBRect[0][0]) / Left.Rect[3]) - float(1.65)) / float(1.65))) #Center Width / Left Height
    Avg.append(abs((((Right.MBRect[0][0] - Left.MBRect[0][0]) / Right.Rect[3]) - float(1.65)) / float(1.65))) #Center Width / Right Height
    
    #Find the outside width to height percent Calculated is 10.25 / 5 = 2.05
    Outside_Width_Top = dist.euclidean((L_P[0][0], L_P[0][1]), (R_P[1][0], R_P[1][1]))
    Outside_Width_Bottom = dist.euclidean((L_P[3][0], L_P[3][1]), (R_P[2][0], R_P[2][1]))
    Avg.append(abs(((Outside_Width_Top / Left.Rect[3]) - float(2.05))/ float(2.05)))
    Avg.append(abs(((Outside_Width_Top / Right.Rect[3]) - float(2.05))/ float(2.05)))
    Avg.append(abs(((Outside_Width_Bottom / Left.Rect[3]) - float(2.05))/ float(2.05)))
    Avg.append(abs(((Outside_Width_Bottom / Right.Rect[3]) - float(2.05))/ float(2.05)))
    
    #Find the Inside width to height ratio Calculated is 6.25 / 5 = 1.25
    Inside_Width_Top = dist.euclidean((L_P[1][0], L_P[1][1]), (R_P[0][0], R_P[0][1]))
    Inside_Width_Bottom = dist.euclidean((L_P[2][0], L_P[2][1]), (R_P[3][0], R_P[3][1]))
    Avg.append(abs(((Inside_Width_Top / Left.Rect[3]) - float(1.25)) / float(1.25)))
    Avg.append(abs(((Inside_Width_Top / Right.Rect[3]) - float(1.25)) / float(1.25)))
    Avg.append(abs(((Inside_Width_Bottom / Left.Rect[3]) - float(1.25)) / float(1.25)))
    Avg.append(abs(((Inside_Width_Bottom / Right.Rect[3]) - float(1.25)) / float(1.25)))
    
    #Find the Diagonal width to height ratio Calculated is 11.4045 / 5 = 2.281
    Diagonal_Width_TL_BR = dist.euclidean((L_P[0][0], L_P[0][1]), (R_P[2][0], R_P[2][1]))
    Diagonal_Width_TR_BL = dist.euclidean((R_P[1][0], R_P[1][1]), (L_P[3][0], L_P[3][1]))
    Avg.append(abs(((Diagonal_Width_TL_BR / Left.Rect[3]) - float(2.281)) / float(2.281)))
    Avg.append(abs(((Diagonal_Width_TL_BR / Right.Rect[3]) - float(2.281)) / float(2.281)))
    Avg.append(abs(((Diagonal_Width_TR_BL / Left.Rect[3]) - float(2.281)) / float(2.281)))
    Avg.append(abs(((Diagonal_Width_TR_BL / Right.Rect[3]) - float(2.281)) / float(2.281)))
    
    #Find the Diagonal width to outside width ratio Calculated is 11.4045 / 10.25 = 1.113
    Avg.append(abs(((Diagonal_Width_TL_BR / Outside_Width_Top) - float(1.113)) / float(1.113)))
    Avg.append(abs(((Diagonal_Width_TL_BR / Outside_Width_Bottom) - float(1.113)) / float(1.113)))
    Avg.append(abs(((Diagonal_Width_TR_BL / Outside_Width_Top) - float(1.113)) / float(1.113)))
    Avg.append(abs(((Diagonal_Width_TR_BL / Outside_Width_Bottom) - float(1.113)) / float(1.113)))
    
    
    Avg.append(Left.Percent_Error_Rectangle/100)
    Avg.append(Right.Percent_Error_Rectangle/100)
        
    Percent_Error = (100 / len(Avg)) * sum(Avg)

    cv2.putText(frame, "{:.1f}".format(Left.Area), (int(Left.MBRect[0][0]),int(Left.MBRect[0][1])), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1)
    cv2.putText(frame, "{:.1f}".format(Right.Area), (int(Right.MBRect[0][0]),int(Right.MBRect[0][1])), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1)
        
    if Percent_Error < 30.00:
        return (Left, Right, Percent_Error)
    else:
        return (Left, Right, 100.0)

#This will compare two or more targets to determine if they meet the
#criteria for a valid target by comparing width and height ratios
def Compare_Multiple_Target_Rect( Screen_Obj ):

    Best_Match_Left = 0
    Best_Match_Right = 0
    Previous_Percent_Error = 100.0
    count = 0
    for T1, T2 in itertools.combinations(Screen_Obj, 2):
        (Left, Right, Percent_Error) = Two_Target_Compare(T1,T2)
        if Percent_Error < Previous_Percent_Error:
            Previous_Percent_Error = Percent_Error
            Best_Match_Left = Left
            Best_Match_Right = Right
            
    return (Best_Match_Left, Best_Match_Right, Previous_Percent_Error)

#This will compare individual rectangle properties to determine if it meets the criteria
#for a valid rectangle
def Compare_Individual_Rect( Screen_Obj ):
    #determine if the rectangles we want here
    Rectangles = []
    Avg_Error = []
    for obj in Screen_Obj:
        Perp_Box = perspective.order_points(obj.Box)
        #The single rectangles do not have much to compare except for the H/W ratio
        #and the diagonal upper left lower right and height ratio.  The height is
        #compared in most of these because it is the longest visible feature on
        #the tape
        
        #If the width is greater than the height don't even bother going further
        if obj.Rect[2] > obj.Rect[3]:
            continue

        
        #The Height Vs Width of a target rectangle should be 5 / 2 = 2.5
        #We will use that to determine how close we are to 2.5
        #Top Width
        TW = dist.euclidean((Perp_Box[0][0], Perp_Box[0][1]), (Perp_Box[1][0], Perp_Box[1][1]))
        BW = dist.euclidean((Perp_Box[3][0], Perp_Box[3][1]), (Perp_Box[2][0], Perp_Box[2][1]))
        LH = dist.euclidean((Perp_Box[0][0], Perp_Box[0][1]), (Perp_Box[3][0], Perp_Box[3][1]))
        RH = dist.euclidean((Perp_Box[1][0], Perp_Box[1][1]), (Perp_Box[2][0], Perp_Box[2][1]))
        Avg_Error.append(abs(((LH/TW ) - float(2.5)) / float(2.5)))
        Avg_Error.append(abs(((LH/BW ) - float(2.5)) / float(2.5)))
        Avg_Error.append(abs(((RH/TW ) - float(2.5)) / float(2.5)))
        Avg_Error.append(abs(((RH/BW ) - float(2.5)) / float(2.5)))
        

        L2R_Diag = dist.euclidean((Perp_Box[0][0], Perp_Box[0][1]), (Perp_Box[2][0], Perp_Box[2][1]))
        R2L_Diag = dist.euclidean((Perp_Box[1][0], Perp_Box[1][1]), (Perp_Box[3][0], Perp_Box[3][1]))
        Avg_Error.append(abs( ((L2R_Diag/LH) - float(1.077))/float(1.077)))
        Avg_Error.append(abs( ((L2R_Diag/RH) - float(1.077))/float(1.077)))
        Avg_Error.append(abs( ((R2L_Diag/LH) - float(1.077))/float(1.077)))
        Avg_Error.append(abs( ((R2L_Diag/RH) - float(1.077))/float(1.077)))
        

        obj.Percent_Error_Rectangle = (100 / len(Avg_Error)) * (sum(Avg_Error))

        if obj.Percent_Error_Rectangle <= 30.00:
            Rectangles.append(obj)

    return Rectangles
    

def nothing(x):
    pass

 

# Creating track bar
cv2.createTrackbar('h_l', 'result',0,179,nothing)
cv2.createTrackbar('h_h', 'result',179,179,nothing)
cv2.createTrackbar('s_l', 'result',0,255,nothing)
cv2.createTrackbar('s_h', 'result',255,255,nothing)
cv2.createTrackbar('v_l', 'result',149,255,nothing)
cv2.createTrackbar('v_h', 'result',255,255,nothing)
while(1):

    _, frame = cap.read()

    #converting to HSV    
    hsv = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)

    # get info from track bar and appy to result
    h_low = cv2.getTrackbarPos('h_l','result')
    s_low = cv2.getTrackbarPos('s_l','result')
    v_low = cv2.getTrackbarPos('v_l','result')

    lower_color = np.array([h_low,s_low,v_low])

    h_Hi = cv2.getTrackbarPos('h_h','result')
    s_Hi = cv2.getTrackbarPos('s_h','result')
    v_Hi = cv2.getTrackbarPos('v_h','result')

    upper_color = np.array([h_Hi,s_Hi,v_Hi])

    mask = cv2.inRange(hsv,lower_color, upper_color)
    result = cv2.bitwise_and(frame,frame,mask = mask)

    kernel = np.ones((3,3),np.uint8)
    mask = cv2.blur(mask.copy(),(3,3))
    mask = cv2.erode(mask.copy(),kernel,iterations=2)
    mask = cv2.dilate(mask.copy(),kernel,iterations=3)
    thresh = cv2.threshold(mask, 50, 255, cv2.THRESH_BINARY)[1]
    cv2.imshow('mask',thresh)
    cnts = cv2.findContours(thresh,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)[-2]
    Saved_Screen_Obj = []
    offset = 0
    for c in cnts:
        area = cv2.contourArea(c)
        rect = (x,y,w,h) = cv2.boundingRect(c) #bounding rect data is (x,y,w,h)
        min_Rect = cv2.minAreaRect(c) #minAreaRect data is ((Center x,Center y),(w,h),angle)
        box = cv2.boxPoints(min_Rect)
        
        if area > 250 and h > w:
            Saved_Screen_Obj.append(Screen_Obj(rect,min_Rect,box,area,0))
            
    #Sort this list by the largest area to smallest area
    Saved_Screen_Obj.sort(key=lambda SO: SO.Area, reverse=True)
    
    #This will vet each individual rectangle to determine if it meets
    #our criteria and can be part of a target
    Interesting_Rectangles = Compare_Individual_Rect(Saved_Screen_Obj)
    Interesting_Rectangles.sort(key=lambda SO: SO.Percent_Error_Rectangle )
        
    if len(Interesting_Rectangles) >= 2:
        (Left, Right, Percent) = Compare_Multiple_Target_Rect(Interesting_Rectangles)
        if Left and Right:
            Left.Box = perspective.order_points(Left.Box)
            Right.Box = perspective.order_points(Right.Box)
            cv2.line(frame,(Left.Box[0][0],Left.Box[0][1]),(Right.Box[1][0],Right.Box[1][1]),(25,57,234),4)
            cv2.line(frame,(Right.Box[1][0],Right.Box[1][1]),(Right.Box[2][0],Right.Box[2][1]),(25,57,234),4)
            cv2.line(frame,(Right.Box[2][0],Right.Box[2][1]),(Left.Box[3][0],Left.Box[3][1]),(25,57,234),4)
            cv2.line(frame,(Left.Box[3][0],Left.Box[3][1]),(Left.Box[0][0],Left.Box[0][1]),(25,57,234),4)

            DL = dist.euclidean((Left.Box[0][0], Left.Box[0][1]), (Right.Box[1][0], Right.Box[1][1]))

            #This value is found by
            # F = (P * D) / W           where:
            # F is the focal length of the camera
            # P is the number of pixels at a known distance
            # D is the known distance from the front of the camera
            # W is the actual measurement in D units
            # On the RPi camera at a distance of 18"
            # the target of 10.25" had a length of 212 pixels
            # So: F = (212 * 24)/10.25
            # then multiply F * 10.25
            inches =  5088 / float(DL)

            cv2.putText(frame, "{:.2f}ft".format(inches / 12), (frame.shape[1] - 200, frame.shape[0] - 20), cv2.FONT_HERSHEY_SIMPLEX, 1.75, (0, 255, 0), 2)
        
    elif len(Interesting_Rectangles) >= 1:
        cv2.drawContours(frame,[np.int0(Interesting_Rectangles[0].Box)],0,(0,255,145),1)

        DH = dist.euclidean((box[0][0], box[0][1]), (box[1][0], box[1][1]))

        #This value is found by
        # F = (P * D) / W           where:
        # F is the focal length of the camera
        # P is the number of pixels at a known distance
        # D is the known distance from the front of the camera
        # W is the actual measurement in D units
        # On the RPi camera at a distance of 18"
        # the target of 10.25" had a length of 212 pixels
        # So: F = (212 * 24)/5
        # then multiply F * 5
        inches =  1200 / float(DH)

        cv2.putText(frame, "{:.2f}ft".format(inches / 12), (frame.shape[1] - 200, frame.shape[0] - 20), cv2.FONT_HERSHEY_SIMPLEX, 1.75, (0, 255, 0), 2)
                

    cv2.imshow('result',frame)

    k = cv2.waitKey(100) & 0xFF
    if k == 27:
        break
    counter += 1

cap.release()
cv2.destroyAllWindows()
